﻿using System.Xml.Linq;
using static System.Console;

static class Program
{
    public static void Main()
    {

        char @continue = 'y';

        do
        {
            int border = InputValue(nameof(border));

            int count = InputValue(nameof(count));

            Write("Enter first word: ");
            string firstWord = ReadLine();
            Clear();

            Write("Enter second word: ");
            string secondWord = ReadLine();
            Clear();

            int a = InputValue(nameof(a));

            int b = InputValue(nameof(b));
           
            Print(border, count, firstWord, secondWord, a, b);

            WriteLine();
            WriteLine("Press 'y' or 'Y' if want to continue.");
            @continue = char.Parse(ReadLine());
            Clear();

        } while (@continue is 'y' or 'Y');

        WriteLine("Finished!");
        ReadKey();
    }

    private static void Print(int borderValue, int count, string firstWord, string secondWord, int a, int b)
    {
        for (int number = 1; number <= borderValue; number++)
        {
            string value = CreateValue(firstWord, secondWord, a, b, number);
            string separator = CreateSeparator(borderValue, count, number);
            Write($"{value}{separator}");
        }
    }

    private static string CreateValue(string firstWord, string secondWord, int a, int b, int number)
        => (number % a) switch
        {
            0 when number % b == 0 => $"{firstWord}{secondWord}",
            0 => firstWord,
            _ => number % b == 0 ? secondWord : $"{number}",
        };

    private static string CreateSeparator(int borderValue, int count, int number)
    {
        return (number % count) switch
        {
            0 when number == borderValue => string.Empty,
            0 => $", {Environment.NewLine}",
            _ => number == borderValue ? string.Empty : ", ",
        };
    }

    //vs Expression-bodied syntax

    //private static string CreateSeparator(int borderValue, int count, int number)
    //    => (number % count) switch
    //    {
    //        0 when number == borderValue => string.Empty,
    //        0 => $", {Environment.NewLine}",
    //        _ => number == borderValue ? string.Empty : ", ",
    //    };

    private static int InputValue(string name)
    {
        int value;

        while (true)
        {
            Write($"Enter {name} > 0: ");
            value = int.Parse(ReadLine());
            Clear();
            if (value > 0)
            {
                return value;
            }

            WriteLine($"Invalid {name}. Try again.");
        }
    }
}